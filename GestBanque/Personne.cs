﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestBanque
{
    public class Personne
    {
        #region Champs
        public string _Nom { get; set; }
        public string _Prenom { get; set; }
        public DateTime _DateNaissance { get; set; }
        private List<Courant> comptes = new List<Courant>();
        #endregion

        public string NomComplet
        {
            get
            {
                return $"{_Prenom} {_Nom}";
            }
        }

        public List<Courant> getComptes()
        {
            return this.comptes;
        }
    }
}
