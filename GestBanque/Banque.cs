﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestBanque
{
    public class Banque
    {
        #region Champs
        private Dictionary<string, Courant> _Comptes;
        #endregion

        #region Props
        public string Nom { get; set;  }
        public Dictionary<string, Courant> Comptes
        {
            get 
            { 
                return _Comptes ?? (_Comptes = new Dictionary<string, Courant>()); 
            }
        }
        #endregion

        #region Indexeur
        public Courant this[string numero]
        {
            get
            {
                // return Comptes.GetValueOrDefault(numero);
                Comptes.TryGetValue(numero, out Courant c);
                return c;
            }
        }
        #endregion


        public void Ajouter(Courant compte)
        {
            if(compte is null || Comptes.ContainsKey(compte.Numero))
            {
                // TODO envoyer une erreur
                return;
            }
            Comptes.Add(compte.Numero, compte);
        }

        public void Supprimer(string numero)
        {
            Comptes.Remove(numero);
        }

        public double AvoirDesComptes(Personne titulaire)
        {
            double sum = 0.0;
            foreach(Courant c in _Comptes.Values)
            {
                if(c.Titulaire.Equals(titulaire))
                {
                    sum += c;
                }
            }

            return sum;
        }
    }
}
