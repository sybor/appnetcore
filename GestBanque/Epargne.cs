﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestBanque
{
    class Epargne : Compte
    {
        private DateTime _DateDernierRetrait;

        public DateTime DateDernierRetrait
        {
            get { return DateDernierRetrait; }
            set { DateDernierRetrait = value; }
        }

    }
}
