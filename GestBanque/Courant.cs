﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestBanque
{
    public class Courant
    {
        public string Numero { get; private set; }
        private double _Solde = 0;

        private double _LigneDeCredit;

        public Personne Titulaire { get; set; }

        public double getSolde()
        {
            return _Solde;
        }

        public double getLigneDeCredit()
        {
            return _LigneDeCredit;
        }
        
        public void setLigneDeCredit(double LigneDeCredit)
        {
            if(LigneDeCredit >= 0)
            {
                this._LigneDeCredit = LigneDeCredit;
            }
        }

        public void Retrait(double Montant)
        {
            if(Montant <= 0)
            {
                return;
            }

            if(_Solde - Montant < -_LigneDeCredit)
            {
                this._Solde -= Montant;
            }
        }

        public void Depot(double Montant)
        {
            this._Solde += Montant;
        }

        public static double operator + (double solde, Courant c2)
        {
            double sum = 0.0;
            if(solde >= 0)
            {
                sum = solde + c2.getSolde();
            }
            return sum;
        }
    }
}
