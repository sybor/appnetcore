﻿using System;

namespace GestBanque
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start Exo!");

            Personne p1 = new Personne();
            p1._Prenom = "Zaza";
            p1._Nom = "Vanderquack";
            p1._DateNaissance = new DateTime(2010, 01, 06);

            Courant compte = new Courant();
            compte.Titulaire = p1;
            compte.Depot(1000);


            AfficherCompte(compte);

            //--------------------------
        }

        private static void AfficherCompte(Courant c)
        {
            Console.WriteLine($"{c.Numero} {c.getSolde()} - {c.Titulaire.NomComplet}");
            Console.WriteLine();
        }
    }
}
